import unittest
import selenium.webdriver as webdriver
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from config import ConfigSectionMap

cfg = ConfigSectionMap("values")


class FastNextCreatingJD(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome("/Users/keshavreddy/PycharmProjects/FastNextUITesting/chromedriver")

    def test_createJD(self):
        self.driver.get(cfg['url'])
        wait = ui.WebDriverWait(self.driver, 10)
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['login']))
        self.loginElement = self.driver.find_element_by_xpath(cfg['login'])
        self.loginElement = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, cfg['login'])))
        self.loginElement.click()
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['email']))
        log = self.driver.find_element_by_xpath(cfg['email'])
        log = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, cfg['email'])))
        log.send_keys(cfg['emailid'])
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['password']))
        log = self.driver.find_element_by_xpath(cfg['password'])
        log.send_keys(cfg['password_value'])
        self.driver.find_element_by_xpath(cfg['login_submit']).click()
        actions = ActionChains(self.driver)
        actions.pause(1)
        actions.send_keys(Keys.TAB * 3)
        actions.pause(2)
        actions.send_keys(Keys.ENTER)
        actions.perform()
        parent = self.driver.find_element_by_class_name('modal-content')
        title = parent.find_element_by_id('title')
        title.send_keys('Python Job Search')
        desc = parent.find_element_by_id('description')
        minexp = parent.find_element_by_id('min_exp')
        minexp.send_keys(3)
        maxexp = parent.find_element_by_id('max_exp')
        maxexp.send_keys(5)
        skills = parent.find_element_by_class_name('input')
        skills.send_keys('python')
        desc.send_keys(cfg['jd'])
        submit = self.driver.find_element_by_xpath(cfg['jd_submit'])
        action = ActionChains(self.driver)
        action.move_to_element(submit)
        action.click()
        action.perform()
        wait.until(lambda driver: self.driver.find_element_by_xpath(cfg['jd_card']))
        text = self.driver.find_element_by_xpath(cfg['jd_card']).text
        self.assertIn('Python Job Search', text)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()