import unittest
from FastNextAdvanceSearchFlow import FastNextAdvanceSearch
from FastNextCreatingJD import FastNextCreatingJD
from FastNextBasicSearch import FastNextBasicSearch
from FastnextLoginFlow import FastNextLogin




if __name__=="__main__":
    suite1 = unittest.TestLoader().loadTestsFromTestCase(FastNextLogin)
    suite2 = unittest.TestLoader().loadTestsFromTestCase(FastNextBasicSearch)
    suite3 = unittest.TestLoader().loadTestsFromTestCase(FastNextAdvanceSearch)
    suite4 = unittest.TestLoader().loadTestsFromTestCase(FastNextCreatingJD)
    suite = unittest.TestSuite([suite1, suite2, suite3, suite4])
    unittest.TextTestRunner().run(suite)
