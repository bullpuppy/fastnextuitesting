import unittest
import selenium.webdriver as webdriver
import selenium.webdriver.support.ui as ui
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from config import ConfigSectionMap

cfg = ConfigSectionMap("values")


class FastNextLogin(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome("/Users/keshavreddy/PycharmProjects/FastNextUITesting/chromedriver")

    def test_login_logout(self):
        self.driver.get(cfg['url'])
        wait = ui.WebDriverWait(self.driver, 10)
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['login']))
        self.loginElement = self.driver.find_element_by_xpath(cfg['login'])
        self.loginElement = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, cfg['login'])))
        self.loginElement.click()
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['email']))
        log = self.driver.find_element_by_xpath(cfg['email'])
        log = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, cfg['email'])))
        log.send_keys(cfg['emailid'])
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['password']))
        log = self.driver.find_element_by_xpath(cfg['password'])
        log.send_keys(cfg['password_value'])
        self.driver.find_element_by_xpath(cfg['login_submit']).click()
        wait.until(lambda driver:driver.find_element_by_id('logout'))
        actions = ActionChains(self.driver)
        actions.send_keys(Keys.TAB * 5)
        time.sleep(2)
        actions.send_keys(Keys.ENTER)
        actions.perform()
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['login']))
        self.assertTrue(self.driver.find_element_by_id('login'), 'login')

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()