import unittest
import selenium.webdriver as webdriver
import selenium.webdriver.support.ui as ui
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time
from config import ConfigSectionMap

cfg = ConfigSectionMap("values")

class FastNextAdvanceSearch(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome("/Users/keshavreddy/PycharmProjects/FastNextUITesting/chromedriver")


    def test_login(self):
        self.driver.get(cfg['url'])
        wait = ui.WebDriverWait(self.driver, 10)
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['login']))
        self.loginElement = self.driver.find_element_by_xpath(cfg['login'])
        self.loginElement = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, cfg['login'])))
        self.loginElement.click()
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['email']))
        log = self.driver.find_element_by_xpath(cfg['email'])
        log = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, cfg['email'])))
        log.send_keys(cfg['emailid'])
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['password']))
        log = self.driver.find_element_by_xpath(cfg['password'])
        log.send_keys(cfg['password_value'])
        self.driver.find_element_by_xpath(cfg['login_submit']).click()
        self.driver.implicitly_wait(3)
        wait.until(lambda driver: self.driver.find_element_by_xpath(cfg['search']).is_enabled())
        search = self.driver.find_element_by_xpath(cfg['search'])
        actions = ActionChains(self.driver)
        actions.send_keys(Keys.TAB * 2)
        actions.send_keys(Keys.ENTER)
        actions.perform()
        # search.click()
        wait.until(lambda driver: driver.find_element_by_class_name('input'))
        skills = self.driver.find_element_by_class_name('input')
        skills.send_keys("python")

        skills.send_keys(Keys.RETURN)
        exp = self.driver.find_element_by_css_selector(cfg['minexp'])
        exp.send_keys('2')
        exp = self.driver.find_element_by_css_selector(cfg['maxexp'])
        exp.send_keys('4')
        parent = self.driver.find_element_by_id("location")
        location = parent.find_element_by_tag_name('input')
        location.send_keys("Bengaluru")
        self.driver.find_element_by_xpath(cfg['place_popup']).click()
        button = self.driver.find_element_by_xpath(cfg['search_submit'])
        button.click()
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['cards']))
        self.assertTrue(self.driver.find_element_by_xpath(cfg['cards']),cfg['cards'])


    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()