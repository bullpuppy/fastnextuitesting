import unittest
import selenium.webdriver as webdriver
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from config import ConfigSectionMap

cfg = ConfigSectionMap("values")


class FastNextBasicSearch(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome("/Users/keshavreddy/PycharmProjects/FastNextUITesting/chromedriver")

    def test_basicSearch(self):
        self.driver.get(cfg['url'])
        wait = ui.WebDriverWait(self.driver, 10)
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['login']))
        self.loginElement = self.driver.find_element_by_xpath(cfg['login'])
        self.loginElement = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, cfg['login'])))
        self.loginElement.click()
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['email']))
        log = self.driver.find_element_by_xpath(cfg['email'])
        log = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, cfg['email'])))
        log.send_keys(cfg['emailid'])
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['password']))
        log = self.driver.find_element_by_xpath(cfg['password'])
        log.send_keys(cfg['password_value'])
        self.driver.find_element_by_xpath(cfg['login_submit']).click()
        wait.until(lambda driver: self.driver.find_element_by_class_name('input'))
        skills = self.driver.find_element_by_class_name('input')
        skills.send_keys("python")
        skills.send_keys(Keys.RETURN)
        parent = self.driver.find_element_by_id("location")
        location = parent.find_element_by_tag_name('input')
        location.send_keys("Bengaluru")
        exp = self.driver.find_element_by_id('exp')
        exp.send_keys(4)
        location.click()
        self.driver.find_element_by_xpath(cfg['basic_search_button']).click()
        wait.until(lambda driver: driver.find_element_by_xpath(cfg['cards']))
        actions = ActionChains(self.driver)
        actions.send_keys(Keys.TAB * 4)
        actions.send_keys(Keys.ENTER)
        actions.perform()
        wait.until(lambda driver: self.driver.find_element_by_xpath(cfg['basic_search']))
        text = self.driver.find_element_by_xpath(cfg['basic_search']).text
        self.assertIn('Basic Search', text)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()